FROM openjdk:11-jdk-buster AS build
WORKDIR /app
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash -
RUN apt install -y nodejs build-essential
RUN npm install -g npm@10.7.0
COPY .eclipse ./.eclipse
COPY gradle ./gradle
COPY typings ./typings
COPY webpack ./webpack
COPY src ./src
COPY include ./include
COPY package* gradle* settings.gradle build.gradle components.json tsconfig.json ./
RUN ./gradlew war

FROM jetty:9.4.50-jre11
LABEL maintainer="LINCS zacanbot@gmail.com"
USER root

ENV DIST_HOME=./dist/docker/platform

# for backward compatibility change uid and guid to the one that was in the alpine based image
# by default on debian id 100 is reserved by _apt user
# WARNING, it can have consequences on the work of apt,apt-get.
RUN usermod -u 1100 _apt
RUN usermod -u 100 jetty
RUN groupmod -g 101 jetty

# add user jetty to group root which is used for most of the permissions
# user jetty has uid 100
# group root has gid 0
RUN addgroup jetty root

# make group 'root' (no special permissions!) owning group of the various jetty base, home, and temp folders
# to allow the container to be run by non-root and non-jetty users as well (this is required e.g. for OpenShift)
# also set same permissions as for owner
RUN echo "fixing group ownership for Jetty base, home, and temp folders" \
	&& umask 0002 \
	&& chgrp -R 0 "$TMPDIR" \
	&& chmod -R g=u "$TMPDIR" \
	&& chmod -R g+s "$TMPDIR" \
	&& chgrp -R 0 "$JETTY_BASE" \
	&& chmod -R g=u "$JETTY_BASE" \
	&& chmod -R g+s "$JETTY_BASE" \
	&& chgrp -R 0 "$JETTY_HOME"\
	&& chmod -R g=u "$JETTY_HOME" \
	&& chmod -R g+s "$JETTY_HOME"

# Custom apps can be mounted under /apps folder
RUN echo "setting up platform, apps, and runtime folders" \
	&& umask 0002 \
	&& mkdir /firstStart \
	&& chown -R 100:0 "/firstStart" \
	&& chmod -R g=u "/firstStart" \
	&& chmod -R g+ws "/firstStart" \
	&& mkdir /apps \
	&& chown -R 100:0 "/apps" \
	&& chmod -R g=u "/apps" \
	&& chmod -R g+ws "/apps" \
	&& mkdir /runtime-data \
	&& mkdir -p /runtime-data/config/repositories \
	&& mkdir -p /runtime-data/data/repositories \
	&& chown -R 100:0 "/runtime-data" \
	&& chmod -R g=u "/runtime-data" \
	&& chmod -R g+ws "/runtime-data" \
	&& mkdir /var/lib/jetty/logs \
	&& chown -R 100:0 "/var/lib/jetty/logs" \
	&& chmod -R g=u "/var/lib/jetty/logs" \
	&& chmod -R g+ws "/var/lib/jetty/logs"

COPY --chown=100:0 $DIST_HOME/entrypoint.sh /
RUN chmod a+x /entrypoint.sh
COPY --chown=100:0 $DIST_HOME/jetty-logging.properties /var/lib/jetty/resources/jetty-logging.properties
COPY --chown=100:0 $DIST_HOME/shiro-tools-hasher-1.3.2-cli.jar /firstStart/
COPY --chown=100:0 $DIST_HOME/first-passwd-init.sh /firstStart/

#COPY --chown=100:0 $DIST_HOME/ROOT.xml /var/lib/jetty/webapps/ROOT.xml
COPY --from=build --chown=100:0 /app/build/libs/ROOT-*.war /var/lib/jetty/webapps/ROOT.war
RUN apt-get update \
  && apt-get install unzip -y
RUN mkdir /var/lib/jetty/webapps/ROOT
RUN unzip /var/lib/jetty/webapps/ROOT.war -d /var/lib/jetty/webapps/ROOT
RUN rm /var/lib/jetty/webapps/ROOT.war
RUN chmod 777 -R /var/lib/jetty/webapps/ROOT/WEB-INF/classes/languages/

# COPY --from=build --chown=100:0 config/shiro.ini /runtime-data/config/shiro.ini
# copy migrations scripts into docker image 
COPY --chown=100:0 ./dist/migrations /migrations

# one final, recursive change of permissions in /runtime-data
RUN chown -R 100:0 "/runtime-data" \
	&& chmod -R g=u "/runtime-data" \
	&& chmod -R g+ws "/runtime-data"

USER jetty

# enable jetty logging configuration module
RUN java -jar "$JETTY_HOME/start.jar" --add-to-start="logging-jetty"

ENV PLATFORM_OPTS=
ENV RUNTIME_OPTS "-DruntimeDirectory=/runtime-data -Dorg.researchspace.config.baselocation=/runtime-data/config -DappsDirectory=/apps -Dconfig.environment.shiroConfig=/runtime-data/config/shiro.ini -Dlog4j.configurationFile=classpath:org/researchspace/logging/log4j2.xml -Dorg.eclipse.jetty.server.Request.maxFormContentSize=104857600"

ENTRYPOINT ["/entrypoint.sh"]
VOLUME /runtime-data
