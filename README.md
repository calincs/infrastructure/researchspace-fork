# Researchspace Fork

This project is a fork from the official repo [here](https://github.com/researchspace/researchspace).

## Prerequisites

It is possible to use a unix-based OS or Windows for development against the platform. As prerequisites you need to have installed on your machine:

* JDK 11 or 17
* Latest Node.js LTS (18.x) is recommended, but any newer Node.js version should be also fine.

In particular, on OSX and Unix systems the most stable versions for Node.js are usually available from common package managers (e.g. homebrew, apt) and as such easy to install and to upgrade.

On Windows the use of [Chocolatey](https://chocolatey.org/) is highly recommended.

### Prerequisites Installation on *Ubuntu*

`sudo apt install openjdk-17-jdk`

`curl -sL https://deb.nodesource.com/setup_18.x | sudo -E bash -`
`sudo apt install nodejs npm`

### Prerequisites Installation on *MacOS*

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

`brew tap AdoptOpenJDK/openjdk`
`brew cask install adoptopenjdk17`

`brew install node@18`

### Prerequisites Installation on *Windows 10*

See [installation instruction](https://chocolatey.org/docs/installation) for [Chocolatey](https://chocolatey.org).

`choco install adoptopenjdk17 -y`

`choco install nodejs-lts -y`

## Running the ResearchSpace in Development Mode

### Run ResearchSpace with your own triplestore and IIIF server

Start Blazegraph and Cantaloupe with Docker Compose or however you wish to run them. Adjust `sparqlEndpoint`, `iiifScaler` and `imageStorageRoot` properties in the `gradle.properties`. Then enter the following into your terminal:

For Linux and Mac:

```sh
./gradlew run
```

For Windows:

```cmd
.\gradlew.bat run
```

You should see console output similar to:

```text
21:24:49 INFO  Jetty 9.4.24.v20191120 started and listening on port 10214
21:24:49 INFO   runs at:
21:24:49 INFO    http://localhost:10214/
```

### Testing

> :warning: **WARNING**: Chromium or Google Chrome browser is required for client-side tests.

Running `./gradlew test` command will execute all backend tests (Java JUnit) as well as client-side unit tests (using mainly mocha, chai, sinon). To just execute the client-side test, you can also run `npm run test`. We also have a number of integration tests, see `integration-tests`.

### Debugging

#### Backend

Run `./gradlew debug` for standalone platform. Once the gradle console displays the message "Listening for transport dt_socket at address: 5005" you can connect to the respective port using, for example, the remote debugging functionality in Eclipse (Run -> Debug Configurations .. -> New "Remote Java Application" -> Choose a name, `localhost` as host and `5005` as port parameter).

#### Frontend

You can use standard browser developer tools for debugging the frontend. Furthermore, there is a dedicated plugin called "React Developer Tools" (available for Chrome, Firefox** for debugging and tracing states of React components.

There are convenient specifics when running in the development mode: Source maps are being attached (`webpack://./src`) i.e. you can debug in the Typescript code instead of the compiled JS code.

### Backend Logging

The platform's backend is using log4j2 (and respective adapters) for logging. It is setup with four pre-configured log profiles.
The default profile is "debug", however, the profile can easily be switched by supplying the `build.sh -Dlog={log4j2-debug,log4j2-trace,log4j2-trace2,log4j2}` environment variable in the gradle console. The `log4j2-trace` and `log4j2-trace2` profile produce a lot of log messages, however, they can be particularly useful when one needs to trace, for example, request headers or queries without goint to debug low level APIs.

## Building WAR artefact

To build ResearchSpace WAR artefact that can be deployed into a Java Servlet Container (like jetty), execute the following command, replace `VERSION_NUMBER` with some valid [semantic versioning](https://semver.org/) number.

`./gradlew clean war`

When the packaging process is complete you will find the .war file in `build/libs/ROOT-VERSION_NUMBER.war`.

## Build zip bundle

It is possible to build ResearchSpace bundled into a simple runnable zip archive together with blazegraph and digilib: `./gradlew clean buildZip`

## Building Docker image

The creation of the platform Docker image consists of packaging the ResearchSpace platform as a Java webapp (the .war file we have just created) in a Java servlet container, Jetty.

The Dockerfile is located in `dist/docker` folder. This file contains the instructions for building the platform's Docker image that is based on an official Jetty server as image.

To build the image, we first need to copy artefacts produced by the platform build process.

```bash
export DOCKER_FOLDER="$(pwd)/dist/docker"
cp build/libs/ROOT-*.war $DOCKER_FOLDER/platform/ROOT.war
mkdir $DOCKER_FOLDER/platform/config
cp src/main/resources/org/researchspace/apps/default/config/shiro.ini $DOCKER_FOLDER/platform/config/
```

Then we can build the image:

```bash
cd $DOCKER_FOLDER/platform
docker build -t researchspace:VERSION_TAG .
```

## Setup IDE

You can use various IDEs and text editors like Eclipse, IDEA, VSCode, Emacs an VIM to work on the project.

While there exist some add-ons for JavaScript and Typescript in Eclipse, it is in principle possible to develop everything in only one IDE, e.g. Eclipse or IDEA. However, in particular for the JavaScript/TypeScript development it can be convenient to use editors such as VSCode, Emacs or VIM with much more powerful plugins.

### Eclipse

If you are used to develop in Eclipse, you can automatically generate a new Eclipse project by executing the `./gradlew eclipse`, which is located in the project root folder.
The command will first resolve all required dependencies and then will automatically generate the classpath file as well as required Eclipse metadata files. Finally, you can import the project into your Eclipse Workspace using the "Existing Projects into Workspace" wizard.

### VSCode

There is a predefined workspace configuration in the `.vscode/settings.json`, so the project can be directly opened in the VSCode.

We recommend to install the following plugins:

* [prettier plugin](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
* [eslint plugin](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

When developing frontend code in the Visual Studio Code we recommend setting TypeScript compiler to locally installed one by clicking on compiler version number in the status bar while viewing any `*.ts` file.

## Code Style & Linting

### Java

Generated eclipse project has predefined code formatter that is automatically enabled. Please always reformat your java code with eclipse before submitting a PR.

For Visual Studio Code users, follow this [guide](https://github.com/redhat-developer/vscode-java/wiki/Formatter-settings) to setup the formatter.

### Typescript & SCSS

We use [Prettier](https://prettier.io/) as code formatter for all `ts/tsx/js/scss` files. And [typescript-eslint](https://typescript-eslint.io) as a linter for typescript files.

## Generate JSON Schema from JSDoc

To generate generate JSON schema from any TypeScript interface to use in the documentation with `mp-documentation`, execute the following command:

`npm run generateJsonSchema <interface-name>`

## Troubleshooting

### Security certificate issues when building the platform

If you are working in an institutional network and experience difficulties at the build stage due to security certificate errors, you may need to add your institution's security certificate to your computer's key-chain, and to the keystore in your Java installation. For example, you may experience errors when trying to download Maven dependencies.

To add the certificate to your java keystore:

`keytool -import -alias example -keystore  /path/to/cacerts -file example.der`

You may also need to disable strict ssl settings in yarn:

`npm config set strict-ssl "false"`
