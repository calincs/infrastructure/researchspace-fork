/**
 * ResearchSpace
 * Copyright (C) 2020, © Trustees of the British Museum
 * Copyright (C) 2015-2019, metaphacts GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { ReactElement, cloneElement, Children, Props as ReactProps, createFactory, createElement, MouseEvent } from 'react';

import * as ReactBootstrap from 'react-bootstrap';
import * as D from 'react-dom-factories';
import * as assign from 'object-assign';

import { Component } from 'platform/api/components';
import { Rdf } from 'platform/api/rdf';
import { SparqlClient } from 'platform/api/sparql';
import { LdpService } from 'platform/api/services/ldp';
import { refresh, navigateToResource } from 'platform/api/navigation';
import { getOverlaySystem } from 'platform/components/ui/overlay';


interface Props extends ReactProps<RemoveFromContainerComponent> {
  container: string;
  iri: string;

  /**
   * @default 'reload'
   */
  postAction?: 'reload' | string;

  /**
   * @default 'true'
   */
  confirmationpopup?: true | false;
}

class RemoveFromContainerComponent extends Component<Props, {}> {
  constructor(props: Props, context: any) {
    super(props, context);
  }

  public static defaultProps = {
    postAction: 'reload',
    confirmationpopup: true
  };  

  public render() {
    const child = Children.only(this.props.children) as ReactElement<any>;
    const props = {
      onClick: (this.props.confirmationpopup === true ? this.popUpConfirmation : this.deleteItem),
    };
    return cloneElement(child, props);
  }

  private modalReference = 'delete-confirmation'

  popUpConfirmation = () => {
    getOverlaySystem().show(
      this.modalReference,
      createElement(DeleteMapModal, {
        onHide: () => getOverlaySystem().hide(this.modalReference),
        onDelete: this.deleteItem,
        show: true,
        title: "Are you sure?",
        body: "Are you sure you want to delete this?",
      })
    );
  }

  deleteItem = () => {
    new LdpService(this.props.container, this.context.semanticContext)
      .deleteResource(Rdf.iri(this.props.iri))
      .onValue(() => {
        if (this.props.postAction === 'reload') {
          refresh();
        } else {
          navigateToResource(Rdf.iri(this.props.postAction)).onValue((v) => v);
        }
      });
    
      getOverlaySystem().hide(this.modalReference)
  };

}


const Modal = createFactory(ReactBootstrap.Modal);
const ModalHeader = createFactory(ReactBootstrap.Modal.Header);
const ModalTitle = createFactory(ReactBootstrap.Modal.Title);
const ModalBody = createFactory(ReactBootstrap.Modal.Body);
const ModalFooter = createFactory(ReactBootstrap.Modal.Footer);
const Button = createFactory(ReactBootstrap.Button);

export interface ConfirmDeletionModalProps extends ReactBootstrap.ModalDialogProps {
  title: string;
  body: string;
  onHide: () => void;
  onDelete: () => void;
  show?: boolean;
}

class DeleteMapModal extends Component<ConfirmDeletionModalProps, {}>{
  render(){
    return (
      Modal(
        assign({}, this.props, {
          onHide: this.props.onHide,
        }),
        ModalHeader({ closeButton: true }, ModalTitle({}, this.props.title)),
        ModalBody(
          {},
          this.props.body,
        ),
        ModalFooter(
          {},
          D.div(
            {
              className: 'pop-up-buttons',
            },
            Button(
              {
                className: 'btn btn-primary',
                onClick: this.props.onDelete,
              },
              'Delete'
            )
          )
        )
      )
    )
  }
}

export type component = RemoveFromContainerComponent;
export const component = RemoveFromContainerComponent;
export const factory = createFactory(component);
export default component;
