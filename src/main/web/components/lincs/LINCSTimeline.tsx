import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as _ from 'lodash';
import * as uuid from 'uuid';

import { Component, ComponentProps, ComponentContext } from 'platform/api/components';
import { SparqlClient } from 'platform/api/sparql';
import { DataSet, Timeline } from 'vis';
import { AnonymousHiddenComponentClass } from '../security/AnonymousHidden';
import { AddToClipBoardContainer } from './AddToClipBoardContainer';
import { ResourceLinkContainer } from 'platform/api/navigation/components';
import { Cancellation } from 'platform/api/async';
import { ResourceLabel } from 'platform/components/ui/resource-label';
import { WithTypes } from 'platform/components/ui/WithTypes' 

export interface LINCSTimelineConfig {
    query: string;
}
export type LINCSTimelineProps = LINCSTimelineConfig & React.Props<LINCSTimeline> & ComponentProps;

interface LINCSTimelineState {
    items: DataSet<any>;
    yearSidebar: Array<Array<any>>;
    parsedDates: Array<any>;
    timeline: any;
}

export class LINCSTimeline extends Component<LINCSTimelineProps, LINCSTimelineState> {
    private readonly cancellation = new Cancellation();
    context: ComponentContext;
    timelineRef = React.createRef<HTMLDivElement>();
    styleId = uuid.v1();

    /**
     * Initializes state
     * 
     * @param props 
     * @param context 
     */
    constructor(props: LINCSTimelineConfig, context: ComponentContext) {
        super(props, context);
        this.state = { items: new DataSet([]), yearSidebar: [], parsedDates: [], timeline: null};
    }

    /**
     * Upon rendering, trigger class method 'onSparqlFetch'. Happens only once after initial render
     */
    public componentDidMount(){
        this.onSparqlFetch()
    }

    /**
     * Upon a re-render (due to change in props or state), check to see if the props have changed such that it may trigger a new sparqlFetch
     * via class method 'onSparqlFetch'
     * 
     * @param {LINCSTimelineProps} prevProps - The previous props before re-render. The query property is the only important comparable. 
     */
    public componentDidUpdate(prevProps: LINCSTimelineProps){
        if (this.props.query !== prevProps.query){
            const timelineDiv = this.timelineRef.current.querySelector("#lincs-timeline")
            timelineDiv.textContent = '';
            // const childTimeline = timelineDiv.querySelector(".vis-timeline")
            // if (childTimeline){
            //     timelineDiv.removeChild(childTimeline)
            // }
            this.onSparqlFetch()
        }
    }

    /**
     * Upon initial render or re-render, this will render divs that other functions will use to inject their HTML code into such as 'vrtimeline' and 'lincs-timeline'
     * 
     * 
     * @returns A react-element (div)
     */
    public render() {
        if (document.querySelector("#style-" + this.styleId.split("-")[0]) == null) {
            document.head.insertAdjacentHTML("beforeend", "<style class='vrstyleBlock' id='style-" + this.styleId.split("-")[0] + "'>#timeline-" + this.styleId.split("-")[0] + "::before {height: 0px;}</style>");
        }

        return (
            <div style={{ display: "flex", flexDirection: "row" }} ref={this.timelineRef}>
                <div id="lincs-timeline" style={{ width: "66%", margin: "2%", marginBottom: "0px", border: "1px solid lightgray", flexGrow: 4 }}>
                </div> {/* THIS DIV'S CHILD GIVES US THE TIMELINE */}
                <div style={{ backgroundColor: "lightgrey", width: "30%", marginTop: "2%", height: "500px" }}>  {/* THIS DIV GIVES US A VERTICAL TIMELINE W/ GRAY BACKGROUND */}
                    <div id="sidebarContent" style={{ height: "500px", overflowY: "auto", padding:"20px" }}>
                        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                            <h3 className="sidebarTitle">Timeline</h3>
                        </div>
                        <div className="vertical-timeline-contents">
                          <div className="line"></div>  {/* THIS GIVES US THE TIMELINE LINE */}
                          <div id={"timeline-" + this.styleId.split("-")[0]} style={{ width: "100%" }}></div> {/* THIS DIV'S CHILD GIVES US THE VERT TIMELINE */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    /**
     * An event handler for horizontal timeline cards where upon click, renders an unordered list with vertical timeline cards as list items. 
     * 
     * @param {string} year - The year that indexes into an array of timeline cards
     */
    private openVerticalTimeline(year) { // This function opens the sidebar timeline
        let listContainer = this.timelineRef.current.querySelector("#timeline-" + this.styleId.split("-")[0]);
        ReactDOM.unmountComponentAtNode(listContainer);
        let renderComponent = (
            <ul id="vrlist" style={{ width: "100%", listStyleType: "none", paddingInlineStart: "0px" }}>
                {this.state.yearSidebar[year]}
            </ul>
        );
        ReactDOM.render(renderComponent, listContainer);
        document.querySelector("#style-" + this.styleId.split("-")[0]).innerHTML = "#timeline-" + this.styleId.split("-")[0] + "::before {height: " + this.timelineRef.current.querySelector("#vrlist").scrollHeight + "px;}";
    }

    /**
     * Uses vis to generate a horizontal timeline at div with class name 'lincs-timeline'.
     */
    private createTimeline() {
        let options = {
            height: '500px',
            zoomMin: 378683420000,
        };
        let timeline = new Timeline(this.timelineRef.current.querySelector("#lincs-timeline"), this.state.items, options);
    }

    /**
     * Fetches entity start/end dates, entity iri, and entity type iri from Sparql query to help create vertical and horizontal timeline.
     * It triggers 'createTimeline' to render a timeline with cards (called DateCard) that triggers 'openVerticalTimeline' to render a vertical timeline on card click.
     * It uses state and props variables to render such timelines
     * 
     * @returns 
    */
    private onSparqlFetch() { {/* SOMEWHERE HERE GIVES THE TIMELINE SOME DATA FOR VIS TO CREATE A TIMELINE */}
        if (document.querySelector("#loading-block") == null) {
            document.body.insertAdjacentHTML("beforeend", `<div id="loading-block" class="modal-backdrop in">
                                                                <span class="system-spinner">
                                                                    <i class="system-spinner__icon"></i>
                                                                    <span class="system-spinner__message">Please wait...</span>
                                                                </span>
                                                            </div>`);
        } else {
            (document.querySelector("#loading-block") as HTMLElement).style.display = "block";
        }

        // TRIGGER THE SPARQL QUERY VIA THE SPARQLCLIENT MODULE
        SparqlClient.select(this.props.query).onValue(
            (response) => { 
                this.setState({items: new DataSet([]), yearSidebar: [], parsedDates: []})
                let years = [];

                {/* GET RESULT.BINDINGS BASED OFF OF THE SPARQL QUERY WE JUST GAVE */}
                for (const date of response.results.bindings) { {/*  */}
                    let year = date.start.value.split("-")[0];
                    if (!years.includes(year)) {
                        this.state.parsedDates[year] = [];
                        years.push(year);
                    }

                    this.state.parsedDates[year].push({date: date.start.value, typeIri: date.type.value, subjectUri: date.subject.value, eventUri: date.event.value})
                }

                for (let i = 0; i < years.length; i++) { 
                    let year = years[i];

                    let dateCard = document.createElement('div');
                    let dateCardInternal = (
                        <div style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                            <h4 className="yearInfo"> {year} </h4>
                            <img onClick={() => { this.openVerticalTimeline(year) }} className="stackButton" src="/assets/components/timeline/imgs/stack.png" style={{ cursor: "pointer", height: "40px", width: "49px", padding: "8px" }} />
                            <span style={{ position: "absolute", right: "16.5px", top: "22px", pointerEvents: "none" }}>{this.state.parsedDates[year].length}</span>
                        </div>
                    );
                    ReactDOM.render(dateCardInternal, dateCard);
                    
                    // THIS IS FOR THE SIDE TIMELINE
                    this.state.yearSidebar[year] = [];
                    for (let y = 0; y < this.state.parsedDates[year].length; y++) {
                        console.log(this.state.parsedDates[year][y])
                        const backgroundColor = this.fetchTypeColor(this.state.parsedDates[year][y].typeIri)
                        this.state.yearSidebar[year].push(
                            <li style={{marginBottom: "10px", float: "none"}}>
                                <div className="vertical-timeline-card-wrapper">
                                  <div className="header">
                                    <div className="point"></div>
                                    <h1 className="date">{year}</h1>
                                  </div>
                                  <div className="body" style={{borderTop: `5px solid ${backgroundColor}`}}>
                                    <div className='title'>
                                      <WithTypes iri={this.state.parsedDates[year][y].eventUri} types={[this.state.parsedDates[year][y].typeIri]} fromTimeline></WithTypes>
                                      <ResourceLabel iri={this.state.parsedDates[year][y].eventUri} className="label"></ResourceLabel>
                                    </div>
                                    <div className="button-grid">
                                      <button className="button" title="Explore Entity">
                                        <ResourceLinkContainer uri={ this.state.parsedDates[year][y].eventUri } target="_self">
                                            <i className="rs-icon rs-icon-page icon"></i>
                                            <span className="label">Explore</span>
                                        </ResourceLinkContainer>
                                      </button>
                                      <button className="button" title="View Knowledge Map">
                                        <ResourceLinkContainer uri="http://www.researchspace.org/resource/ThinkingFrames" urlqueryparam-view="knowledge-map" urlqueryparam-resource={ this.state.parsedDates[year][y].eventUri } draggable={false} target="_blank">
                                            <i className="rs-icon rs-icon-diagram icon"></i>
                                            <span className="label">Map</span>
                                        </ResourceLinkContainer>
                                      </button>
                                      <AnonymousHiddenComponentClass>
                                        <AddToClipBoardContainer iri={this.state.parsedDates[year][y].eventUri} iconOnly></AddToClipBoardContainer>
                                      </AnonymousHiddenComponentClass>
                                    </div>
                                  </div>
                                </div>
                            </li>
                        );
                    }
                    this.state.items.add({ id: i, content: dateCard, start: year + "-01-01" });
                }

                this.createTimeline()
                setTimeout(() => {
                    (document.querySelector("#loading-block") as HTMLElement).style.display = "none";
                }, 1000);
            }
        );
    }
    
    /**
     * 
      * Finds hex colours based off of Entity type (iri). It mimics to mapping of TypeMapping.html (template) to certain classes in css_variables.css
      *
      * @param {string} iri - The type iri that we use to match type to colour
      * @returns {string} The matched colour based off iri in 'typeMap' private variable
    */
    private fetchTypeColor(iri: string): string{
        for (const a in this.typeMap){
            const type: {"key": string, "value": string} = this.typeMap[a]
            if (iri === type.key){
                return type.value
            }
        }

        //default if there is no iri that matches
        return "#AAAAAA"
    }

    //THIS IS SUPPOSED TO MIMIC LINCSTypeToCard in ...TypeMappings.html and css_variables.css in rsp/apps/lincs (the rsp app)
    //CHANGES APPLIED THERE SHOULD BE APPLIED HERE TO ENSURE CONSISTENT CARD BORDERING
    private typeMap = [
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E67_Birth",
                "value": "#100CC0"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E69_Death",
                "value": "#340570"
              },
              {
                "key": "http://iflastandards.info/ns/fr/frbr/frbroo/F31_Performance",
                "value": "#9947EB"
              },
              {
                "key": "http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event",
                "value": "#9947EB"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E12_Production",
                "value": "#F28CE2"
              },  
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E65_Creation",
                "value": "#F28CDC"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E7_Activity",
                "value": "#E0A3F5"
              },
              {
                "key": "http://iflastandards.info/ns/fr/frbr/frbroo/F51_Pursuit",
                "value": "#CD6CEF"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E13_Attribute_Assignment",
                "value": "#EF6C9B"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E8_Acquisition",
                "value": "#EF6CDA"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/crmtex/TX6_Transcription",
                "value": "#F00F7B"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E33_E41_Linguistic_Appellation",
                "value": "#EC7651"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E33_Linguistic_Object",
                "value": "#EC9B51"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E36_Visual_Item",
                "value": "#ECAE51"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E42_Identifier",
                "value": "#ECD351"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E56_Language",
                "value": "#DFEC51"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E73_Information_Object",
                "value": "#F5CAA3"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E89_Propositional_Object",
                "value": "#C1CDA7"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E55_Type",
                "value": "#889C5C"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E57_Material",
                "value": "#D0F075"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E54_Dimension",
                "value": "#F3FDD9"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E97_Monetary_Amount",
                "value": "#BAD864"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object",
                "value": "#51EC57"
              },
              {
                "key": "http://iflastandards.info/ns/fr/frbr/frbroo/F4_Manifestation_Singleton",
                "value": "#33A437"
              },
              {
                "key": "http://www.ics.forth.gr/isl/CRMdig/D1_Digital_Object",
                "value": "#8FE34D"
              },
              {
                "key": "http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression",
                "value": "#40C946"
              },
              {
                "key": "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work",
                "value": "#60D264"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E53_Place",
                "value": "#71D6BE"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E39_Actor",
                "value": "#C3EDFF"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E74_Group",
                "value": "#3D51FF"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E21_Person",
                "value": "#3D96FF"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E9_Move",
                "value": "#D846C0"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E28_Conceptual_Object",
                "value": "#CC9766"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E29_Design_or_Procedure",
                "value": "#E9900C"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span",
                "value": "#EDDC82"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit",
                "value": "#9DC723"
              },
              {
                "key": "http://iflastandards.info/ns/fr/frbr/frbroo/F28_Expression_Creation",
                "value": "#F28CDC"
              },
              {
                "key": "http://www.cidoc-crm.org/cidoc-crm/E78_Curated_Holding",
                "value": "#67AE04"
              }
            ]
}
export default LINCSTimeline;
