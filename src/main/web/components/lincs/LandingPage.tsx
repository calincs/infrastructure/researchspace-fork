import * as React from "react";
import { SparqlClient } from 'platform/api/sparql';
import { ComponentContext, ComponentProps } from "platform/api/components/";
import { ResourceLinkContainer } from "platform/api/navigation/components";
import './LandingPage.scss'
import LincsImageCarousel from "./LincsImageCarousel";
import { CopyToClipboardComponent } from "../copy-to-clipboard";
import { TemplateItem } from 'platform/components/ui/template';

export interface LandingPageConfig {
    title: string
}
export type LandingPageProps = LandingPageConfig & React.Props<LandingPage> & ComponentProps;

export interface LandingPageState {
    description: React.ReactElement;
    disclaimer: React.ReactElement;
    logoImageArr: React.ReactElement;
    teamMembers: React.ReactElement;
    affiliatedLinks: React.ReactElement;
    editor: boolean;
}

export class LandingPage extends React.Component<LandingPageProps, LandingPageState> {
    context: ComponentContext;
    constructor(props: LandingPageConfig, context: ComponentContext) {
        super(props, context);
        this.state = {
            description: <p>Loading...</p>,
            disclaimer: <p></p>,
            logoImageArr: <p>Loading...</p>,
            teamMembers: <p>Loading...</p>,
            affiliatedLinks: <div></div>,
            editor: false,
        };
    }

    // Function to render the top left description
    private descriptionRender = (response: any) => {
        return (
            <p className="people-details" id="fontSpecifier">
                {this.renderString(response.results.bindings[0].description.value)}
            </p>
        )
    }

    // Function to render the disclaimer
    private disclaimerRender = (response: any) => {
        return (
            <p className="disclaimer" id="fontSpecifier">
                DISCLAIMER: {response.results.bindings[0].disclaimer.value}
            </p>
        )
    }
    

    // Function to render the team members
    private teamMembersRender = (response: any) => {
        return (
            <div>
                {response.results.bindings.map((name : any, index : number) => (
                        <p className="people-details">{name.display.value}</p>
                    )
                )}
            </div>
        )
    }

    // Function to render the images of the bottom container
    private logoImagesRender = (response: any) => {
        return (
            <div className="image-all-container">
                {response.results.bindings.map((logo : any, index : number) => {
                        if (logo.relativeFilePath.value !== "/assets/images/Orlando.png") {
                            return <img className="logo-image" src={logo.relativeFilePath.value} alt={`${index} Image`} />
                        }
                    }
                )}
            </div>
        )
    }

    // Function to render the affiliated links
    private affiliatedLinksRender = (response: any) => {
        return (
            <div>
                {
                    response.results.bindings[0].app != null &&
                    <div>
                        <div className="link-padding"> 
                            <h3>Application Profile</h3>
                                <a className="landing-page-link" href={response.results.bindings[0].app.value} target="_blank">
                                    {response.results.bindings[0].app.value} <i className="fa fa-external-link"></i>
                            </a>
                        </div>
                    </div>
                }
                {
                    response.results.bindings[0].affiliatedWebsite != null &&
                    <div>
                        <div className="link-padding"> 
                            <h3>Affiliated Website</h3>
                                <a className="landing-page-link" href={response.results.bindings[0].affiliatedWebsite.value} target="_blank">
                                    {response.results.bindings[0].affiliatedWebsite.value} <i className="fa fa-external-link"></i>
                            </a>
                        </div>
                    </div>
                }
                {
                    response.results.bindings[0].sourceURL != null &&
                    <div>
                        <div className="link-padding">   
                            <h3>Source URL</h3>
                            <a className="landing-page-link" href={response.results.bindings[0].sourceURL.value} target="_blank">
                                {response.results.bindings[0].sourceURL.value} <i className="fa fa-external-link"></i>
                            </a>
                        </div>
                    </div>
                }
                {
                    response.results.bindings[0].archivalURL != null &&
                    <div>
                        <div className="link-padding"> 
                            <h3>Archive URL</h3>
                            <a className="landing-page-link" href={response.results.bindings[0].archivalURL.value} target="_blank">
                                {response.results.bindings[0].archivalURL.value} <i className="fa fa-external-link"></i>
                            </a>
                        </div>
                    </div>
                }
            </div>
        )
    }

    private renderString = (templateString?: string): React.ReactElement<any> => {
        return React.createElement(TemplateItem, {
            template: {
                source: templateString,
            }
        });
    };

    public componentDidMount() {

        // Rendering the top left disclaimer
        SparqlClient.select(`SELECT distinct ?disclaimer where { 
            graph <http://metadata.lincsproject.ca> {
           ?subject crmdig:L43_annotates <http://graph.lincsproject.ca/${this.props.title}> ;
           crm:P2_has_type <http://id.lincsproject.ca/datasetAnnotation> ;
           crm:P190_has_symbolic_content ?disclaimer . 
           }}
        `).onValue((res) => {
            if (res.results.bindings.length == 0) {
                this.setState({disclaimer: null});
            } else { 
                this.setState({
                    disclaimer: this.disclaimerRender(res)
                })
            }
        })

        // Rendering the top left description
        SparqlClient.select(`SELECT ?description WHERE { GRAPH <http://metadata.lincsproject.ca> {
            ?linguistic_object crm:P129_is_about <http://graph.lincsproject.ca/${this.props.title}> ;
                <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> <http://vocab.getty.edu/aat/300411780>  ;
                <http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content>  ?description .
          }}
        `).onValue((res) => {
            if (res.results.bindings.length == 0) {
                this.setState({description: null});
            } else { 
                this.setState({
                    description: this.descriptionRender(res)
                })
            }
        })

        // Rendering bottom images
        SparqlClient.select(`SELECT ?relativeFilePath WHERE { GRAPH <http://metadata.lincsproject.ca> {
            ?image <http://www.cidoc-crm.org/cidoc-crm/P138_represents> <http://graph.lincsproject.ca/${this.props.title}> ;
                <http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by> ?identifier .
            ?identifier <http://www.researchspace.org/ontology/PX_has_file_name> ?filename .
            BIND (CONCAT("/assets/images/", ?filename) as ?relativeFilePath)
          }}
        `).onValue((res) => {
            if (res.results.bindings.length == 0) {
                this.setState({logoImageArr: null});
            } else { 
                this.setState({
                    logoImageArr: this.logoImagesRender(res)
                })
            }
        })

        // Rendering team members
        SparqlClient.select(`SELECT ?display WHERE { GRAPH <http://metadata.lincsproject.ca> {
            ?creation crmdig:L22_created_derivative <http://graph.lincsproject.ca/${this.props.title}> ;
                crm:P14_carried_out_by ?creator .
            ?creator rdfs:label ?label .
            ?carried_out crm:P02_has_range ?creator ;
                 crm:P01_has_domain ?creation ;
                 crm:P14.1_in_the_role_of ?role .
            ?role rdfs:label ?role_label .
            bind(CONCAT(CONCAT(?label, " - "), ?role_label) as ?display)
          }}`).onValue((res) => {
            if (res.results.bindings.length == 0) {
                this.setState({teamMembers: null});
            } else { 
                this.setState({
                    teamMembers: this.teamMembersRender(res)
                })
            }
        })

        // Rendering external links
        SparqlClient.select(`SELECT ?affiliatedWebsite ?sourceURL ?archivalURL ?app WHERE { GRAPH <http://metadata.lincsproject.ca> {
            OPTIONAL { 
                ?derivative crm:P130_shows_features_of <http://graph.lincsproject.ca/${this.props.title}> ;
                    crm:P1_is_identified_by ?sourceId .
                ?sourceId crm:P190_has_symbolic_content ?sourceURL . 
            }
            OPTIONAL { 
                <http://graph.lincsproject.ca/${this.props.title}> <http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by> ?archival_id . 
			    ?archival_id <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> <http://id.lincsproject.ca/archivalURL> ;
                <http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content> ?archivalURL .
            }
             OPTIONAL { 
                ?app_info crm:P129_is_about <http://graph.lincsproject.ca/${this.props.title}> ;
                    <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> <http://id.lincsproject.ca/applicationProfile> ;
                    <http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by> ?app_id . 
			    ?app_id <http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content> ?app .
            }
            OPTIONAL { 
                ?affiliated_website_info crm:P129_is_about <http://graph.lincsproject.ca/${this.props.title}> ;
                    <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> <http://id.lincsproject.ca/affiliatedWebsite> ;
                    <http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by> ?affiliatedWebsite_id . 
			    ?affiliatedWebsite_id <http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content> ?affiliatedWebsite .
            }
        }}`).onValue((res) => {
            if (res.results.bindings.length == 0) {
                this.setState({affiliatedLinks: null});
            } else { 
                this.setState({
                    affiliatedLinks: this.affiliatedLinksRender(res)
                })
            }
        })

        // Checking for editor permissions
        SparqlClient.ask(`ASK WHERE { GRAPH <http://permissions.lincsproject.ca> { 
            <http://graph.lincsproject.ca/${this.props.title}> lincs:permits ?user .
            ?user  <http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by> ?__useruri__ .
        }}`).onValue((res) => {
            this.setState({
                editor: res
            })
        });
    }
    public render() {
        return (
            <div className="landing-page-container">
                <div className="landing-page-side-container">
                    { 
                        this.state.editor &&
                        <ResourceLinkContainer  uri="http://www.researchspace.org/resource/ThinkingFrames" urlqueryparam-view="entity-editor" urlqueryparam-resource={"http://graph.lincsproject.ca/" + this.props.title}>
                            <a className="landing-page-link">Edit dataset information <i className="fa fa-external-link"></i></a>
                        </ResourceLinkContainer>
                    }
                    <div className="landing-page-module">
                        <h2 className="blue-text">Project Description</h2>
                        {this.state.description}
                    </div>
                    <div className="landing-page-module">
                        {this.state.disclaimer}
                    </div>                    
                    <div className="landing-page-module">
                        {this.state.teamMembers ? <h2 className="blue-text">Creators & Contributors</h2> : null}
                        {this.state.teamMembers}
                    </div>
                </div>
                <div className="landing-page-side-container">
                    <div className="landing-page-module landing-page-image-carousel">
                        <LincsImageCarousel title={this.props.title}></LincsImageCarousel>
                        <h1>Links</h1>
                        <div className="link-container">
                            <CopyToClipboardComponent text={window.location.href} message="The link has been copied to your clipboard.">
                                <a className="landing-page-link">
                                    Link directly to this page <i className="fa fa-copy"></i>
                                </a>
                            </CopyToClipboardComponent>
                            {this.state.affiliatedLinks}
                        </div>
                    </div>
                    <div className="landing-page-module">
                        {this.state.logoImageArr ? <h2 className="blue-text">Sponsors & Affiliations</h2> : null}
                        <div className="sponsors-images-container">
                            {this.state.logoImageArr}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default LandingPage;
