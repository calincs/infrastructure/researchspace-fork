import * as React from "react";
import { ComponentContext, ComponentProps } from "platform/api/components/";
import { TemplateItem } from 'platform/components/ui/template';
import { ResourceLinkComponent } from "platform/api/navigation/components";

export interface RecentDatasetsConfig {
    cookieName: string;
}
export type RecentDatasetsProps = RecentDatasetsConfig & React.Props<RecentDatasets> & ComponentProps;

export interface RecentDatasetsState {
    recentDatasets: Array<RecentDatasetInfo>;
    loadingDatasets: Boolean;
}

export interface RecentDatasetInfo {
    name: string
    thinkingframeUri: string
}

export class RecentDatasets extends React.Component<RecentDatasetsProps, RecentDatasetsState> {
    context: ComponentContext;
    constructor(props: RecentDatasetsConfig, context: ComponentContext) {
        super(props, context);
        this.state = { recentDatasets: [], loadingDatasets: true };
    }
    public componentDidMount() {
        let cookieStr = this.getCookie(this.props.cookieName);
        let cookie = { values: [] };
        if (cookieStr != "") {
            cookie = JSON.parse(this.getCookie(this.props.cookieName));
        }

        let recentDatasets = [];
        for (let dataset of cookie.values) {
            let datasetInfo = dataset.split("~");
            recentDatasets.push({
                name: datasetInfo[1] ?? "",
                thinkingframeUri: datasetInfo[0] ?? ""
            });
        }

        this.setState({ recentDatasets: recentDatasets, loadingDatasets: false });
    }
    
    public render() {
        if (this.state.loadingDatasets) {
            return (
                <p>
                    Loading...
                </p>
            );
        } else if (this.state.recentDatasets.length > 0) {
            return (
                <div style={{display: "flex", flexDirection: "column"}}>
                    {
                        this.state.recentDatasets.map((dataset: RecentDatasetInfo, index: number) => {
                            return (
                                <ResourceLinkComponent 
                                    style={{ color: "#2e5d72", textDecoration: "none", fontSize: "14px", fontWeight: 600, lineHeight: 1.5}} 
                                    iri={ "http://www.researchspace.org/resource/" + dataset.thinkingframeUri }
                                >
                                    { dataset.name }
                                </ResourceLinkComponent>
                            );
                        })
                    }
                </div>
            );
        } else {
            return (
                <p>
                    No recently viewed datasets
                </p>
            );
        }
    }
      
    public getCookie(cname) {
        let name = cname + "=";
        let ca = document.cookie.split(';');
        for(let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}

export default RecentDatasets;