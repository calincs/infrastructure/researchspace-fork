import * as React from "react";
import * as _ from "lodash";
import { SparqlClient } from "platform/api/sparql";
import {
  Component,
  ComponentProps,
  ComponentContext,
} from "platform/api/components";
import { Cancellation } from "platform/api/async";
import { ResourceLinkComponent } from "platform/api/navigation/components";
import "./LincsImageCarousel.scss";

export interface LincsImageCarouselConfig {
  title: string;
}
export type LincsImageCarouselProps = LincsImageCarouselConfig &
  React.Props<LincsImageCarousel> &
  ComponentProps;

interface LincsImageCarouselState {
  imageIndex: number;
  numImage: number;
  animationActive: boolean;
  imageData: any;
  loading: boolean;
}

export class LincsImageCarousel extends Component<LincsImageCarouselProps,LincsImageCarouselState> {
  context: ComponentContext;
  private readonly cancellation = new Cancellation();
  timer: ReturnType<typeof setTimeout>;
  constructor(props: LincsImageCarouselConfig, context: ComponentContext) {
    super(props, context);
    this.state = {
      imageIndex: 0,
      numImage: 0,
      animationActive: false,
      imageData: [],
      loading: true,
    };
    this.timer = null;
  }

  private prevSlide = () => {
    if (this.state.imageIndex == 0) {
      this.setState({ imageIndex: this.state.numImage - 1 });
    } else {
      this.setState({ imageIndex: this.state.imageIndex - 1 });
    }
  };

  private nextSlide = () => {
    if (this.state.imageIndex == (this.state.numImage - 1)) {
      this.setState({ imageIndex: 0 });
    } else {
      this.setState({ imageIndex: this.state.imageIndex + 1 });
    }
  };

  private resetTimer = () => {
    // first stop the timer then call setInterval again to reset the timer
    clearInterval(this.timer);
    this.timer = setInterval(() => {
      this.nextSlide();
    }, 6000);
  };

  private handleLeftArrowClick = () => {
    // we want to reset the timer first so that the autoplay animation feels more fluid
    // but only reset it if the animation autoplay is active
    if (this.state.animationActive) {
      this.resetTimer();
    }
    this.prevSlide();
  };

  private handleRightArrowClick = () => {
    if (this.state.animationActive) {
      this.resetTimer();
    }
    this.nextSlide();
  };

  private toggleAnimationAutoplay = () => {
    if (this.timer == null) return;

    if (this.state.animationActive) {
      clearInterval(this.timer);
    } else {
      this.timer = setInterval(() => {
        this.nextSlide();
      }, 6000);
    }

    this.setState({ animationActive: !this.state.animationActive });
  };

  public componentDidMount() {
    SparqlClient.select(
        `PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
        PREFIX lincs: <http://id.lincsproject.ca/>
        SELECT DISTINCT ?image_iri ?item_iri ?attribution ?date WHERE {
            {
                SELECT ?image_iri ?type WHERE {
                    {
                        GRAPH <http://metadata.lincsproject.ca> {
                        <http://graph.lincsproject.ca/${this.props.title}> crm:P1_is_identified_by ?image_iri.
                        ?image_iri crm:P2_has_type lincs:imageCarousel.
                        BIND("a" AS ?type)
                        }
                    }
                    UNION
                    {
                        GRAPH <http://graph.lincsproject.ca/${this.props.title}> {
                        ?image_iri rdf:type crm:E36_Visual_Item.
                        BIND("b" AS ?type)
                        }
                    }
                }
                ORDER BY (?type)
                LIMIT 3
            }
        
            {
                SELECT ?image_iri (SAMPLE(?itemIri) AS ?item_iri) (SAMPLE(?itemTitle) AS ?item_title) (SAMPLE(?Date) AS ?date) (SAMPLE(?itemCreatorIri) AS ?item_creator_iri) (SAMPLE(?itemCreatorLabel) AS ?item_creator_label) WHERE {
                  { ?image_iri crm:P138_represents ?itemIri . } UNION { ?itemIri crm:P138i_has_representation ?image_iri . }
                  ?itemIri rdfs:label ?itemTitle.
                    OPTIONAL {
                        ?event (crm:P108_has_produced|crm:P94_has_created) ?itemIri .
                        OPTIONAL {
                            ?event crm:P4_has_time-span ?time.
                            ?time crm:P82_at_some_time_within ?Date.
                        }
                        ?event crm:P14_carried_out_by ?itemCreatorIri.
                        ?itemCreatorIri rdfs:label ?itemCreatorLabel.
                    }
                } GROUP BY ?image_iri
            }
        
            BIND(IF(BOUND(?item_creator_label), CONCAT(CONCAT(?item_creator_label, ", "), CONCAT(CONCAT("\\\"", ?item_title), "\\\" ")), ?item_title) AS ?label)
            BIND(IF(BOUND(?date), CONCAT(CONCAT(CONCAT(?label, CONCAT(" ("), ?date), ")")), ?item_title) AS ?attribution)
        }
        ORDER BY ?type
        LIMIT 3`
    ).onValue((res) => {
        if (res.results.bindings.length == 0) {
            this.setState({
                loading: false,
            });
        } else {
            this.setState({
                imageData: res.results.bindings,
                numImage: res.results.bindings.length,
                loading: false,
            });

            if (res.results.bindings.length > 1) {
              this.timer = setInterval(() => {
                this.nextSlide();
              }, 6000);

              this.setState({
                animationActive: true,
              });
            }
        }
    });
  }

  public componentWillUnmount() {
    clearInterval(this.timer);
  }

  public render() {
    if (this.state.loading == false && this.state.imageData.length != 0) {
      return (
        <div className="carousel-container">
          <div className="first-carousel-container">
            <div className="carousel-image-container">
              <img
                className="carousel-image"
                src={this.state.imageData[this.state.imageIndex].image_iri._value}
                id="animations"
              />
            </div>
            <div className="attribution-container">
              <ResourceLinkComponent className="attribution-link" iri={this.state.imageData[this.state.imageIndex].item_iri._value}>
                {this.state.imageData[this.state.imageIndex].attribution._value}
              </ResourceLinkComponent>
            </div>
          </div>
          <div className="second-carousel-container">
            <button className="noStyle" onClick={this.handleLeftArrowClick}>
              <div className="left-arrow"></div>
            </button>
            {this.state.animationActive ? (
              <button
                className="toggle-button"
                onClick={this.toggleAnimationAutoplay}
              >
                <svg
                  fill="#215368"
                  xmlns="http://www.w3.org/2000/svg"
                  height="1em"
                  viewBox="0 0 320 512"
                >
                  <path d="M48 64C21.5 64 0 85.5 0 112V400c0 26.5 21.5 48 48 48H80c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48H48zm192 0c-26.5 0-48 21.5-48 48V400c0 26.5 21.5 48 48 48h32c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48H240z" />
                </svg>
              </button>
            ) : (
              <button
                className="toggle-button"
                onClick={this.toggleAnimationAutoplay}
              >
                <svg
                  fill="#215368"
                  xmlns="http://www.w3.org/2000/svg"
                  height="1em"
                  viewBox="0 0 384 512"
                >
                  <path d="M73 39c-14.8-9.1-33.4-9.4-48.5-.9S0 62.6 0 80V432c0 17.4 9.4 33.4 24.5 41.9s33.7 8.1 48.5-.9L361 297c14.3-8.7 23-24.2 23-41s-8.7-32.2-23-41L73 39z" />
                </svg>
              </button>
            )}
            <button className="noStyle" onClick={this.handleRightArrowClick}>
              <div className="right-arrow"></div>
            </button>
          </div>
          <div className="indicators-container">
            {Array(this.state.numImage).fill(1).map((el, i) =>
              <div className={this.state.imageIndex == i ? "isActive" : "notActive"}></div>
            )}
          </div>
        </div>
      );
    } else if (this.state.loading == false) {
      return (
        <div className="carousel-container">
          <div className="first-carousel-container">
            <div className="carousel-image-container">
              <img
                className="carousel-image"
                src={"/assets/images/LINCS.png"}
                id="animations"
              />
            </div>
          </div>
          <div className="second-carousel-container">
            <button className="noStyle">
              <div className="left-arrow"></div>
            </button>
            <button className="toggle-button">
              <svg
                fill="#215368"
                xmlns="http://www.w3.org/2000/svg"
                height="1em"
                viewBox="0 0 384 512"
              >
                <path d="M73 39c-14.8-9.1-33.4-9.4-48.5-.9S0 62.6 0 80V432c0 17.4 9.4 33.4 24.5 41.9s33.7 8.1 48.5-.9L361 297c14.3-8.7 23-24.2 23-41s-8.7-32.2-23-41L73 39z" />
              </svg>
            </button>
            <button className="noStyle">
              <div className="right-arrow"></div>
            </button>
          </div>
          <div className="indicators-container">
            <div className="isActive"></div>
          </div>
        </div>
      );
    } else {
      return <p>Loading...</p>;
    }
  }
}
export default LincsImageCarousel;
