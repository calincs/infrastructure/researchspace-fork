import * as React from 'react';
import { Component, ComponentProps, ComponentContext } from 'platform/api/components';
import { addToDefaultSet } from 'platform/api/services/ldp-set';
import { Rdf } from 'platform/api/rdf';
import { trigger } from 'platform/api/events';

export interface AddToClipBoardContainerConfig {
    iri: any;
    iconOnly?: boolean
}
export type AddToClipBoardContainerProps = AddToClipBoardContainerConfig & React.Props<AddToClipBoardContainer> & ComponentProps;

interface AddToClipBoardContainerState {
    items: Object;
}
export class AddToClipBoardContainer extends Component<AddToClipBoardContainerProps, AddToClipBoardContainerState> {
    context: ComponentContext;
    constructor(props: AddToClipBoardContainerConfig, context: ComponentContext) {
        super(props, context);
        this.state = { items: props.iri };
    }


    public render() {

        //This is currently present in the vertical timeline
        //Classes 'button' and 'label' are a part of _card.scss
        if (this.props.iconOnly) {
            return (
                <button type="submit" onClick={() => { this.addResourceToClipboard(this.state.items) }} className="rs-authoring-btn button" title="Add to Saved Items">
                    <i className="fa fa-clipboard icon"></i>
                    <span className="label">Save</span>
                </button>
            )
        }
        return(
            <div>
                <button type="submit" onClick={() => { this.addResourceToClipboard(this.state.items) }} className="rs-authoring-btn agregation-info-button-wrapper" title="Add to Saved Items">
                    <i className="fa fa-clipboard agregation-info-button"></i>
                    <span>Add to Saved Items</span>
                </button>
            </div>
        )
    }
    private addResourceToClipboard(iri) {
        addToDefaultSet(Rdf.iri(iri), 'clipboard-trigger').onValue(() => {
            trigger({ eventType: 'Dashboard.OpenSidebar', targets: ["thinking-frames"], source: 'clipboard-trigger' });
            trigger({ eventType: 'Component.Refresh', targets: ["clipboard"], source: 'clipboard-trigger' });
        });

    }

}

export default AddToClipBoardContainer