import * as React from 'react';
import { Component, ComponentProps, ComponentContext } from 'platform/api/components';
import { CopyToClipboardComponent } from '../copy-to-clipboard';

export interface AddResourceLinkToClipboardConfig {
    iri: any;
}
export type AddResourceLinkToClipboardProps = AddResourceLinkToClipboardConfig & React.Props<AddResourceLinkToClipboard> & ComponentProps;

interface AddResourceLinkToClipboardState {
    items: Object;
}
export class AddResourceLinkToClipboard extends Component<AddResourceLinkToClipboardProps, AddResourceLinkToClipboardState> {
    context: ComponentContext;
    constructor(props: AddResourceLinkToClipboardConfig, context: ComponentContext) {
        super(props, context);
        this.state = { items: props.iri };
    }


    public render() {
        return (
            <CopyToClipboardComponent text={window.location.href.split("?")[0] + '?uri=' + this.props.iri} message="The link has been copied to your clipboard.">
                <button type="submit" className="rs-authoring-btn agregation-info-button-wrapper" title="Copy Link to this Entity Page" style={{ width: "60%" }}>
                    <i className="fa fa-clipboard agregation-info-button"></i>
                    <span>Copy Link to this Entity Page</span>
                </button>
            </CopyToClipboardComponent>
        )
    }
}

export default AddResourceLinkToClipboard