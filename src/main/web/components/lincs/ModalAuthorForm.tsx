import * as React from 'react';
import * as _ from 'lodash';
import { Component, ComponentProps, ComponentContext } from 'platform/api/components';
import { TemplateItem } from 'platform/components/ui/template';
import { SparqlClient } from 'platform/api/sparql';
import { FieldDefinitionProp } from 'platform/components/forms/FieldDefinition';
import { InputKind } from 'platform/components/forms/inputs/InputCommpons';
import { PostAction, ResourceEditorForm } from '../forms';
import { Rdf } from 'platform/api/rdf';
import { ResourceLinkComponent } from 'platform/api/navigation/components';

export interface ModalAuthorFormConfig {
    semanticTitle: string;
    semanticTitleIri: string;
    semanticFields: ReadonlyArray<FieldDefinitionProp>;

    subject?: string | Rdf.Iri,
    postAction?: PostAction
}
export type ModalAuthorFormProps = ModalAuthorFormConfig & React.Props<ModalAuthorForm> & ComponentProps;

interface ModalAuthorFormState {
    componentToRender: string;
    renderedComponent: React.ReactElement;
}

export class ModalAuthorForm extends Component<ModalAuthorFormProps, ModalAuthorFormState> {
    public static readonly inputKind = InputKind.ModalAuthorForm;
    context: ComponentContext;
    constructor(props: ModalAuthorFormConfig, context: ComponentContext) {
      super(props, context);
      this.state = {componentToRender: "<p>Loading...</p>", renderedComponent: null};
    }
  
    private newNodeForm(graph) {
        return (
           <div>
                <h1><ResourceLinkComponent iri={this.props.semanticTitleIri}>{this.props.semanticTitle}</ResourceLinkComponent></h1>
                <ResourceEditorForm id='modal-authoring-form' browserPersistence={false} persistence={{type: 'sparql', targetGraphIri: graph}} newSubjectTemplate='http://id.lincsproject.ca/{{UUID}}' subject={this.props.subject} fields={this.props.semanticFields} postAction={this.props.postAction}>
                    {this.props.children}
                </ResourceEditorForm>  
           </div> 
        )
    }

    public componentDidMount() {
        let graphElement = document.querySelector('.entity-graph-for-modal');
        let graph = graphElement.id.split("-");

        SparqlClient.ask(`ASK {
                                GRAPH <http://permissions.lincsproject.ca> {  
                                    ?user crm:P1_is_identified_by ?__useruri__ .
                                    <` + graph[graph.length - 1] + `> <http://id.lincsproject.ca/permits> ?user .
                                }
        }`).onValue(
            (response) => {
                if (!response) {
                    this.setState({componentToRender: "<p>You do not have permission to edit any named graphs. Contact the administrator.</p>"});
                } else {
                    SparqlClient.ask(`ASK {
                                            GRAPH <http://metadata.lincsproject.ca> { 
                                                <` + graph[graph.length - 1] + `> crm:P2_has_type <http://id.lincsproject.ca/prepublication> . 
                                            }
                    }`).onValue(
                        (response) => {
                            if (!response) {
                                this.setState({componentToRender: "<p>No named graphs are available for you to edit. Contact the administrator.</p>"});
                            } else {
                                this.setState({renderedComponent: this.newNodeForm(graph[graph.length - 1])});
                            }
                        }
                    );
                }
            }
        );  
    }

    public render() {
        if (this.state.renderedComponent != null) {
            return this.state.renderedComponent;
        }
        return this.renderResult(this.state.componentToRender);
    }
  
    private renderResult = (templateString?: string): React.ReactElement<any> => {
      return React.createElement(TemplateItem, {
        template: {
          source: templateString,
        }
      });
    };
}
export default ModalAuthorForm;
  