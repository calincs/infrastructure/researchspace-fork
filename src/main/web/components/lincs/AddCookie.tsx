import * as React from "react";
import { ComponentContext, ComponentProps } from "platform/api/components/";

export interface AddCookieConfig {
    cookieName: string;
    addValue: string;
}
export type AddCookieProps = AddCookieConfig & React.Props<AddCookie> & ComponentProps;

export interface AddCookieState {}

export interface Cookie {
    values: Array<string>
}

export class AddCookie extends React.Component<AddCookieProps, AddCookieState> {
    context: ComponentContext;
    constructor(props: AddCookieConfig, context: ComponentContext) {
        super(props, context);
        this.state = {};
    }

    public componentDidMount() {
        let cookieStr = this.getCookie(this.props.cookieName);
        let cookie: Cookie;

        if (cookieStr == "") {
            cookie = {
                values: [
                    this.props.addValue
                ]
            };
        } else {
            cookie = JSON.parse(cookieStr);

            let valueIndex = cookie.values.indexOf(this.props.addValue);
            if (valueIndex != -1) {
                cookie.values.splice(valueIndex, 1);
            }

            cookie.values.unshift(this.props.addValue);
            if (cookie.values.length > 5) {
                cookie.values.pop();
            }
        }

        this.setCookie(this.props.cookieName, JSON.stringify(cookie), 365);
    }
    
    public render() {
        return null;
    }

    public setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        let expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
      
    public getCookie(cname) {
        let name = cname + "=";
        let ca = document.cookie.split(';');
        for(let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}

export default AddCookie;