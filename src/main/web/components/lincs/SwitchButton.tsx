import * as React from "react";
import { ComponentContext, ComponentProps } from "platform/api/components/";

export interface SwitchButtonConfig {
    buttonClass: string,
    firstGroupClass: string,
    secondGroupClass: string
}
export type SwitchButtonProps = SwitchButtonConfig & React.Props<SwitchButton> & ComponentProps;

export interface SwitchButtonState {
    display: Boolean;
}

export class SwitchButton extends React.Component<SwitchButtonProps, SwitchButtonState> {
    context: ComponentContext;
    constructor(props: SwitchButtonConfig, context: ComponentContext) {
        super(props, context);
        this.state = {
            display: true
        };
    }

    public componentDidMount() {
        for (let el of document.getElementsByClassName(this.props.secondGroupClass) as any) {
            el.style.display = 'none';
        }

        for (let el of document.getElementsByClassName(this.props.buttonClass) as any) {
            el.onclick = () => { 
                this.setState({display: !this.state.display});

                if (this.state.display) {
                    el.style.removeProperty('color');

                    for (let el of document.getElementsByClassName(this.props.firstGroupClass) as any) {
                        el.style.removeProperty('display');
                    }
                    for (let el of document.getElementsByClassName(this.props.secondGroupClass) as any) {
                        el.style.display = 'none';
                    }
                } else {
                    el.style.color = 'rgb(0, 71, 96)';

                    for (let el of document.getElementsByClassName(this.props.firstGroupClass) as any) {
                        el.style.display = 'none';
                    }
                    for (let el of document.getElementsByClassName(this.props.secondGroupClass) as any) {
                        el.style.removeProperty('display');
                    }
                }
            };
        }

    }
    
    public render() {
        return(this.props.children);
    }
}

export default SwitchButton;