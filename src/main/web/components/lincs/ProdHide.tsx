import * as React from "react";
import { ComponentContext, ComponentProps } from "platform/api/components/";
import { ConfigHolder } from 'platform/api/services/config-holder';

export interface ProdHideConfig {}
export type ProdHideProps = ProdHideConfig & React.Props<ProdHide> & ComponentProps;

export interface ProdHideState {}

export class ProdHide extends React.Component<ProdHideProps, ProdHideState> {
    context: ComponentContext;
    constructor(props: ProdHideConfig, context: ComponentContext) {
        super(props, context);
    }
    
    public render() {
        if (ConfigHolder.getUIConfig().productionDisplay) {
            return null;
        } else {
            return(this.props.children);
        }
    }
}

export default ProdHide;