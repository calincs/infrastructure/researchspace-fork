import * as React from "react";
import { ComponentContext, ComponentProps } from "platform/api/components/";

export interface ThinkingFrameCheckConfig {
    isThinkingFrame: boolean;
}
export type ThinkingFrameCheckProps = ThinkingFrameCheckConfig & React.Props<ThinkingFrameCheck> & ComponentProps;

export interface ThinkingFrameCheckState {}

export class ThinkingFrameCheck extends React.Component<ThinkingFrameCheckProps, ThinkingFrameCheckState> {
    context: ComponentContext;
    constructor(props: ThinkingFrameCheckConfig, context: ComponentContext) {
        super(props, context);
    }
    
    public render() {
        const isThinkingFrame = document.getElementsByClassName("thinking-frames-page").length > 0;
        if (this.props.isThinkingFrame && isThinkingFrame) {
            return(this.props.children);
        } else if (!this.props.isThinkingFrame && !isThinkingFrame) {
            return(this.props.children);
        } else {
            return null;
        }
    }
}

export default ThinkingFrameCheck;