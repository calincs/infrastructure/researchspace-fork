import * as React from "react";
import { ComponentContext, ComponentProps } from "platform/api/components/";
import { ConfigHolder } from 'platform/api/services/config-holder';

export interface ReviewHideConfig {}
export type ReviewHideProps = ReviewHideConfig & React.Props<ReviewHide> & ComponentProps;

export interface ReviewHideState {}

export class ReviewHide extends React.Component<ReviewHideProps, ReviewHideState> {
    context: ComponentContext;
    constructor(props: ReviewHideConfig, context: ComponentContext) {
        super(props, context);
    }
    
    public render() {
        if (ConfigHolder.getUIConfig().reviewDisplay) {
            return null;
        } else {
            return(this.props.children);
        }
    }
}

export default ReviewHide;