import * as React from 'react';
import * as _ from 'lodash';
import { Component, ComponentProps, ComponentContext } from 'platform/api/components';
import { TemplateItem } from 'platform/components/ui/template';
import { SparqlClient } from 'platform/api/sparql';
import { ResourceEditorForm } from 'platform/components/forms/ResourceEditorForm';
import { SemanticContextProvider } from 'platform/api/components/SemanticContext';
import { HiddenInput } from 'platform/components/forms/inputs/HiddenInput';
import { AutocompleteInput } from './AutocompleteInput';
import { FieldDefinitionProp } from 'platform/components/forms/FieldDefinition';
import { Cancellation } from 'platform/api/async';
import { listen, trigger } from 'platform/api/events';
import { InputUpdatedEvent, InputUpdatedEventData } from './BuiltInEvents';

export interface AuthorFormConfig {
    externalFields: ReadonlyArray<FieldDefinitionProp>;
    externalInsertQuery: string;
    template: string;
    frame: string;
    node?: string;
    tokenizeLuceneQuery?: string;
    escapeLuceneSyntax?: string;
    semanticTitle: string;
    semanticTitleIri: string;
    semanticFields: ReadonlyArray<FieldDefinitionProp>;
}
export type AuthorFormProps = AuthorFormConfig & React.Props<AuthorForm> & ComponentProps;

interface AuthorFormState {
    renderString: Boolean; 
    componentToRender: string;
    renderedComponent: React.ReactElement;
    selectedURI: string;
    selectedGraph: string;
    tabResults: SparqlClient.SparqlSelectResult;
}

export class AuthorForm extends Component<AuthorFormProps, AuthorFormState> {
    context: ComponentContext;
    private readonly cancellation = new Cancellation();
    constructor(props: AuthorFormConfig, context: ComponentContext) {
      super(props, context);
      this.state = {renderString: true, componentToRender: "<p>Loading...</p>", renderedComponent: null, selectedURI: "", selectedGraph: "", tabResults: null};
    }
  
    private newNodeTabs() {
        var tabs = "<bs-tabs id='forms' mount-on-enter='true'>";
        var response = this.state.tabResults;

        for (var i = 0; i < response.results.bindings.length; i++) {
            tabs += "<bs-tab event-key='" + response.results.bindings[i].graphIRI.value + "' title='Add to " + response.results.bindings[i].title.value + "' class='" + response.results.bindings[i].graphIRI.value + "'>";
            tabs += "<div class='entity-graph-for-modal' id='graph-" + response.results.bindings[i].graphIRI.value + "'></div>"
            tabs += "<div class='main_heading'><h1><semantic-link target='_blank' iri='" + this.props.semanticTitleIri + "'>New Entity: " + this.props.semanticTitle + "</semantic-link></h1></div><div class='sub_heading'>Select a property below to edit or add values to this entity.</h2></div>"
            tabs += "<semantic-form id='authoring-form' persistence='{\"type\": \"sparql\", \"targetGraphIri\": \"" + response.results.bindings[i].graphIRI.value + "\"}' new_subject-template = 'http://id.lincsproject.ca/{{{{raw}}}}{{UUID}}{{{{/raw}}}}' post-action='redirect' fields='" + JSON.stringify(this.props.semanticFields) + "'>"
            tabs += this.props.template;
            tabs += "</semantic-form>"
            tabs += "</bs-tab>";
        }

        tabs += "</bs-tabs>";

        this.setState({renderString: true, componentToRender: tabs});
    }

    private editNode(node) {
        var graph;
        SparqlClient.select(`SELECT ?g where { GRAPH ?g { <` + node + `> ?p ?o } } LIMIT 1`).onValue(
            (response) => {
                graph = response.results.bindings[0].g.value;
                SparqlClient.ask(`PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> ASK { 
                                    GRAPH <http://permissions.lincsproject.ca> { 
                                        ?user crm:P1_is_identified_by ?__useruri__ . 
                                        <` + graph + `> <http://id.lincsproject.ca/permits> ?user .
                                    } 
                                }`).onValue(
                    (response) => {
                        if (!response) {
                            this.setState({renderString: true, componentToRender: "<p>You do not have permission to edit this named graph. Contact the administrator.</p>"});
                        } else {
                            SparqlClient.ask(`ASK { 
                                                GRAPH <http://metadata.lincsproject.ca> {<` + graph + `> crm:P2_has_type <http://id.lincsproject.ca/prepublication> . } 
                                            }`).onValue(
                                (response) => {
                                    if (!response) {
                                        this.setState({renderString: true, componentToRender: "<p>This named graph is not available for you to edit. Contact the administrator.</p>"});
                                    } else {
                                        var form = "<h1><semantic-link target='_blank' iri='" + this.props.semanticTitleIri + "'>" + this.props.semanticTitle + "</semantic-link></h1>"
                                        form += "<div class='entity-graph-for-modal' id='graph-" + graph + "'></div>"
                                        form += "<semantic-form id='authoring-form' persistence='{\"type\": \"sparql\", \"targetGraphIri\": \"" + graph + "\"}' subject = '" + node + "' post-action='redirect' fields='" + JSON.stringify(this.props.semanticFields) + "'>"
                                        form += this.props.template;
                                        form += "</semantic-form>"
                                        this.setState({renderString: true, componentToRender: form});
                                    }
                                }
                            );
                        }
                    }
                );
            }
        );
    }

    private externalEntity() {
        if (this.state.selectedURI != "") {
            this.setState({renderString: true, componentToRender: `
            <div id="loading-block" class="modal-backdrop in">
                <span class="system-spinner">
                    <i class="system-spinner__icon"></i>
                    <span class="system-spinner__message">Please wait...</span>
                </span>
            </div>`});
            SparqlClient.construct(
                this.props.externalInsertQuery.replace(/\{iri\}/g, this.state.selectedURI).replace(/\{graphIRI\}/g, this.state.selectedGraph),
                { context: { repository: 'ephedra' } }
            ).onValue(
                () => {
                    trigger({
                        eventType: 'Dashboard.ResourceChanged',
                        source: 'link',
                        targets: [this.props.frame],
                        data: {
                            resourceIri: this.state.selectedURI,
                        }
                    });
        
                    this.editNode(this.state.selectedURI);
                }
            );
        } else {
            alert("No External Entity Selected!");
        }
    }

    private onGraphChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({selectedGraph: (e.target as HTMLInputElement).value});
    }

    private externalLookup(response) {
        this.setState({selectedGraph: response.results.bindings[0].graphIRI.value});
        var _this = this;

        return (
                <SemanticContextProvider repository="ephedra">
                    <ResourceEditorForm fields={this.props.externalFields}>
                        <div className="existing_entity">
                            <div className="add_to_graph">
                                <span className="graphs_label">Add your entity to a dataset selected from the list below:</span>
                                {response.results.bindings.map(function(result) {
                                    return (
                                        <span>
                                            <label>
                                                <input type="radio" value={result.graphIRI.value} defaultChecked={_this.state.selectedGraph === result.graphIRI.value} onChange={_this.onGraphChange} name="graph"/> 
                                                {result.title.value} 
                                            </label>
                                            <br/>
                                        </span>
                                    );
                                })}
                            </div>

                            <div className="external_entity_autocomplete">
                                <HiddenInput for="classtype"></HiddenInput>
                                <AutocompleteInput for="lookup" tokenizeLuceneQuery={this.props.tokenizeLuceneQuery === "false"} escapeLuceneSyntax={this.props.escapeLuceneSyntax === "false"}></AutocompleteInput>
                            </div>

                            <button className="btn btn-default author_form_button" onClick={this.externalEntity.bind(this)}>Add selected entity to the dataset</button>
                        </div>
                        <div className="alternatively">
                            <div className="button_msg">Alternatively,</div>
                            <button className="btn btn-default author_form_button" onClick={this.newNodeTabs.bind(this)}>Create New Entity (New Entity Form)</button>
                        </div>
                    </ResourceEditorForm>
                </SemanticContextProvider>
        );
    }

    public componentDidMount() {

        this.cancellation.map(
            listen({
                eventType: InputUpdatedEvent,
                target: "lincs-author-form",
            })
        ).observe({
                value: ({ data }) => {
                    if((data as InputUpdatedEventData).iri == null) {
                        this.setState({selectedURI: ""});
                    } else {
                        this.setState({selectedURI: (data as InputUpdatedEventData).iri.value._value});
                    }
                },
        });

        if (this.props.node == null) {
            SparqlClient.select(`SELECT (COUNT(?graphIRI) AS ?sum) WHERE {
                                    GRAPH <http://permissions.lincsproject.ca> {  
                                        ?user crm:P1_is_identified_by ?__useruri__ . ?graphIRI <http://id.lincsproject.ca/permits> ?user .
                                    }
                                }`).onValue(
                (response) => {
                    if (response.results.bindings[0].sum.value == "0") {
                        this.setState({renderString: true, componentToRender: "<p>You do not have permission to edit any named graphs. Contact the administrator.</p>"});
                    } else {
                        SparqlClient.select(`SELECT ?graphIRI ?title (COUNT(?graphIRI) AS ?sum)
                                            FROM <http://metadata.lincsproject.ca> 
                                            FROM <http://permissions.lincsproject.ca> 
                                            WHERE { 
                                                ?user crm:P1_is_identified_by ?__useruri__ .
                                                ?graphIRI <http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by> ?titleIdentifier ;
                                                    crm:P2_has_type <http://id.lincsproject.ca/prepublication> ; 
                                                    <http://id.lincsproject.ca/permits> ?user .
                                                ?titleIdentifier <http://www.cidoc-crm.org/cidoc-crm/P2_has_type> <http://vocab.getty.edu/aat/300417209>  ;
                                                    <http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content>  ?title .
                                            } GROUP BY ?graphIRI ?title`).onValue(
                            (response) => {
                                if (response.results.bindings[0].sum.value == "0") {
                                    this.setState({renderString: true, componentToRender: "<p>No named graphs are available for you to edit. Contact the administrator.</p>"});
                                } else {
                                    this.setState({tabResults: response, renderString: false, renderedComponent: this.externalLookup(response)});
                                }
                            }
                        );
                    }
                }
            );
        } else {
            this.editNode(this.props.node);
        }
    }

    public render() {
        if (this.state.renderString == true) { 
            return this.renderResult(this.state.componentToRender);
        } else {
            return this.state.renderedComponent;
        }
    }
  
    private renderResult = (templateString?: string): React.ReactElement<any> => {
      return React.createElement(TemplateItem, {
        template: {
          source: templateString,
        }
      });
    };
}
export default AuthorForm;
  