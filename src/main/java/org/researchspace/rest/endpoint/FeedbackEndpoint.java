package org.researchspace.rest.endpoint;

import java.net.URL;
import java.net.HttpURLConnection;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("")
public class FeedbackEndpoint {

    @POST
    @Path("/feedback")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response postFeedbackForm(
        @FormDataParam("name") String name,
        @FormDataParam("email") String email,
        @FormDataParam("title") String title,
        @FormDataParam("description") String description,
        @FormDataParam("type") String type
    ) {
        try {
            URL url = new URL("https://api.mailjet.com/v3.1/send");
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("Authorization", "Basic NGNjMDUwZDg2Yzc0NDM5ODY4ZDcxZGY3MTUzYWI4MDQ6M2M1MWM5Mjc0OTI0M2M2Yjg3NTVmZjEzYzkxMGE4NjU=");

            String data = "{\"Messages\":[{ \"From\": { \"Email\": \"lincsmailer@gmail.com\", \"Name\": \"" + name + "\" }, " +
                            "\"To\": [{ \"Email\": \"contact-project+calincs-admin-feedback-support@incoming.gitlab.com\", \"Name\": \"Service Desk\" }], " +
                            "\"Subject\": \"" + title + "\", " +
                            "\"TextPart\": \"**Submitter**: " + name + "<br /><br />**Type**: " + type + "<br /><br />" + description + "\", " +
                            "\"CustomID\": \"CreateFeedbackTicket\"," +
                            "\"Headers\": { \"Reply-To\": \"" + email + "\"} }]}";

            byte[] out = data.getBytes(StandardCharsets.UTF_8);

            OutputStream stream = http.getOutputStream();
            stream.write(out);

            System.out.println(http.getResponseCode() + " " + http.getResponseMessage());
            http.disconnect();
        } catch (Exception e) {
            System.out.println(e);
        }

         return Response.ok().build();
    }

}