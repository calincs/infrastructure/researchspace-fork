package org.researchspace.templates.helper;

import java.io.IOException;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.github.jknack.handlebars.Handlebars;
import org.researchspace.config.Configuration;

/**
 * Raw block helper for documentation purpose:
 * https://github.com/jknack/handlebars.java/issues/444
 * 
 * However, right now there is a bug in handlebars.java i.e. raw block helper
 * does not allow to use custom delimiters so you have to mix delimiters: <code>
 * [[{{mainService}}]] 
 * </code>
 * 
 * @author Dawson MacPhee <dmacphee@uoguelph.ca>
 *
 */
public class MainServiceHelper implements Helper<Object> {

    private final Configuration config;

    public MainServiceHelper (Configuration config) {
        this.config = config;
    }

    @Override
    public CharSequence apply(Object context, Options options) throws IOException {
        return new Handlebars.SafeString(config.getEnvironmentConfig().getSparqlEndpoint());
    }

}